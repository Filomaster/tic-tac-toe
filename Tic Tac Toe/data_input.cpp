#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>

#include "struct.h" //Plik zawierający wszelakie enumy
#include "externs.h"
#include "functions.h" //Plik łączności funkcji
using namespace std;

string NewGame;
string Stats;
string Language;
string Themes;
string Exit;
string NewGame_selected;
string Stats_selected;
string Language_selected;
string Themes_selected;
string Exit_selected;
string Language_active;
string Player;
string Player1_turn;
string Player2_turn;
string Scores_title[7];

HIGH_SCORE score_table[20];

LANGUAGE lang = l_ENG;

int x_axis = 2;
int y_axis = 1;
float version = 0.5;

bool ENTER;
bool ESC;
void settings(bool write)
{
	fstream settings;
	settings.open("bin\\settings.txt", ios::in | ios::out);
	if (write == true)
	{
		settings << active_theme;
	}
	else if (write == false)
	{
		settings >> active_theme;
	}
}
void language()
{
	fstream lang_file;

	switch (lang)
	{
	case l_PL:
		lang_file.open("lang\\PL.txt", ios::in);
		break;
	case l_ENG:
		lang_file.open("lang\\ENG.txt", ios::in);
		break;
	}
	getline(lang_file, NewGame);
	getline(lang_file, Stats);
	getline(lang_file, Language);
	getline(lang_file, Themes);
	getline(lang_file, Exit);
	getline(lang_file, NewGame_selected);
	getline(lang_file, Stats_selected);
	getline(lang_file, Language_selected);
	getline(lang_file, Themes_selected);
	getline(lang_file, Exit_selected);
	getline(lang_file, Language_active);
	getline(lang_file, Player);
	getline(lang_file, Player1_turn);
	getline(lang_file, Player2_turn);
	for (int i = 0; i < 7; i++)
	{
		getline(lang_file, Scores_title[i]);
	}

	lang_file.close();
}
void scores()
{
	fstream players_name;
	fstream high_scores;
	fstream dates;
	players_name.open("PLAYERS.txt", ios::in | ios::out);
	high_scores.open("SCORES.txt", ios::in | ios::out);
	dates.open("DATE_FILE.txt", ios::in | ios::out);
	for (int i = 0; i < 20; i++)
	{
		getline(players_name, score_table[i].Nick);
		high_scores >> score_table[i].Score;
		getline(dates, score_table[i].Date);
	}
}
void navigation(int y, int x, bool inverted)
{

	char Key;
	Key = _getch();
	system("cls");
	switch (Key)
	{
	case 72: //Up arrow
		if(inverted == true)
		{ 
			y_axis--;
			if (y_axis < 0)
			{
				y_axis = y;
			}
			
		}
		else {
			y_axis++;
			if (y_axis > y)
			{
				y_axis = 0;
			}
		}
		break;
	case 80: //Down arrow
		if (inverted == true)
		{
			y_axis++;
			if (y_axis > y)
			{
				y_axis = 0;
			}
		}
		else {
			y_axis--;
			if (y_axis < 0)
			{
				y_axis = y;
			}
		}
		break;
	case 75: //Left arrow
		x_axis--;
		if (x_axis < 0)
		{
			x_axis = x;
		}
		break;
	case 77: //Right arrow
		x_axis++;
		if (x_axis > x)
		{
			x_axis = 0;
		}
		break;
	case 27: //ESC
			ESC = true;
		break;
	case 13: //ENTER
		ENTER = true;
		break;
	}
}