#pragma once
#include<iostream>
#include"struct.h"
using namespace std;
extern string NewGame;
extern string Stats;
extern string Language;
extern string Themes;
extern string Exit;
extern string NewGame_selected;
extern string Stats_selected;
extern string Language_selected;
extern string Themes_selected;
extern string Exit_selected;
extern string Language_active;
extern string Player;
extern string Player1_turn;
extern string Player2_turn;
extern string Scores_title[7];

extern bool ENTER;
extern bool ESC;
extern bool EXIT;

extern int x_axis;
extern int y_axis;
extern int colPrim;
extern int colSec;
extern int colThird;
extern int Player1_points;
extern int Player2_points;
extern int active_theme;
extern float version;

extern LANGUAGE lang;
extern HIGH_SCORE score_table[20];
extern PLAYER player;