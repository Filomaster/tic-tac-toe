#pragma once
#include<iostream>

enum LANGUAGE {l_PL, l_ENG};
enum MENU_STATE { m_Game, m_Score, m_Quit};
enum GAMESTATE {g_InGame, g_Win, g_Draw };
enum FIELD_TYPE {field_Empty, field_Cross, field_Circle};
enum FIELD_STATUS {crossed_horizontally, crossed_vertically, crossed_right_slop, crossed_left_slop, blank, selected};
enum PLAYER { Player_X, Player_O, Player_AI};

struct HIGH_SCORE 
{
	std::string Nick;
	int Score;
	std::string Date;
};