/*
------------------------------------------------------------------------------------------------------------------------------------
													  _______ _        _______           _______         
													 |__   __(_)      |__   __|         |__   __|        
												        | |   _  ___     | | __ _  ___     | | ___   ___ 
													    | |  | |/ __|    | |/ _` |/ __|    | |/ _ \ / _ \
												        | |  | | (__     | | (_| | (__     | | (_) |  __/
												        |_|  |_|\___|    |_|\__,_|\___|    |_|\___/ \___|
																												by Filomaster
------------------------------------------------------------------------------------------------------------------------------------
*/

#pragma comment(lib, "winmm.lib")

#include <Windows.h>
#include <mmsystem.h>
#include <iostream>
#include <string>
#include"functions.h"
#include"externs.h"
using namespace std;

HANDLE hOut;
void color(int color_number)
{
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	
		SetConsoleTextAttribute(hOut, color_number);
	
}
int main()
{
	
	system("chcp 1250");
	system("cls");
	//settings(false);
	//cout << active_theme;
	//Sleep(1000);
	language();
	scores();
	
	//PlaySound(TEXT("bin\\music.wav"), NULL, SND_FILENAME | SND_ASYNC);
	startup();
	
	do 
	{
		draw_menu(); //Remember to change it to 'draw_menu' after testing
		
	} while (EXIT == false);
	settings(true);
	//cout << active_theme;
	//Sleep(1000);
	return 0;
}