#pragma once
void language();
void title();
void navigation(int y, int x, bool inverted);
void draw_menu();
void draw_scene();
void settings(bool write);
void startup();
void scores();
void score_board();
void offset(int x);
void start_scene();
void clear_field();
void color(int color_number);