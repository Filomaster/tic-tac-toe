#pragma comment(lib, "winmm.lib")

#include<Windows.h>
#include <iostream>
#include <string>
#include "functions.h"
#include "externs.h"

using namespace std;

int colPrim = 7;
int colSec = 7;
int colThird = 7;
int active_theme = 9;
//11 & 13 pinky theme
char tab = '\t';
bool EXIT = false;


//----------------------- GRAPHIC'S ELEMENTS -----------------------------
void offset(int x)
{
	for (int i = 0; i < x; i++)
	{
		cout << tab;
	}
}
void title()
{
	color(colPrim);
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
	color(colSec);
	cout << "      ::::::::::: ::::::::::: ::::::::       ::::::::::: :::      ::::::::       ::::::::::: ::::::::  ::::::::::" << endl;
	cout << "          :+:         :+:    :+:    :+:          :+:   :+: :+:   :+:    :+:          :+:    :+:    :+: :+:" << endl;
	cout << "          +:+         +:+    +:+                 +:+  +:+   +:+  +:+                 +:+    +:+    +:+ +:+" << endl;
	cout << "          +#+         +#+    +#+                 +#+ +#++:++#++: +#+                 +#+    +#+    +:+ +#++:++#" << endl;
	cout << "          +#+         +#+    +#+                 +#+ +#+     +#+ +#+                 +#+    +#+    +#+ +#+" << endl;
	cout << "          #+#         #+#    #+#    #+#          #+# #+#     #+# #+#    #+#          #+#    #+#    #+# #+#" << endl;
	cout << "          ###     ########### ########           ### ###     ###  ########           ###     ########  ##########" << endl;
	color(colPrim);
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
	cout << "" << endl;
	
}
void sign(int lines)
{
	for (int i = 0; i < lines; i++)
	{
		cout << "" << endl;
	}
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
	offset(5); cout << " Tic Tac Toe�   v. " << version << "   by Filomaster" << endl;
}
//-------------------------------------------------------------------
void score_board()
{
	cout << "" << endl;
	color(colSec);
	for (int j = 0; j < 7; j++)
	{
		offset(4);	cout << Scores_title[j] << endl;
	}
	cout << "" << endl;
	color(colPrim);
	for (int i = 0; i < 20; i++)
	{
		offset(2);
		if (i < 10)
		{
			cout << "0";
		}
		cout << i;
		color(colThird);
		cout << ". " << score_table[i].Nick;
		color(colPrim); cout << " ------------------------------------------------------- "; color(colThird);
		if (score_table[i].Score < 10)
		{
			cout << "000";
		}
		else if (10 < score_table[i].Score < 100)
		{
			cout << "00";
		}
		else if (100 < score_table[i].Score < 1000)
		{
			cout << "0";
		}
		
		cout << score_table[i].Score << " ";
		color(colPrim);  cout<< score_table[i].Date << endl;

	}
}
void languages()
{
	ENTER = false;
	system("cls");
	x_axis = 0;
	title();
	offset(6);  cout << NewGame << endl;
	offset(6);  cout << Stats << endl;
	color(colSec);  offset(6);  cout << Language << endl;
	color(colThird);  offset(6);  cout << Language_active << endl; color(colPrim);
	offset(6); cout << Themes << endl;
	offset(6);  cout << Exit << endl;
	sign(13);
	navigation(0, 1, false);

	switch (x_axis)
	{
	case 0:
		offset(6);  cout << NewGame << endl;
		offset(6);  cout << Stats << endl;
		color(colSec);  offset(6);  cout << Language << endl;
		color(colThird); offset(6);  cout << Language_active << endl; color(colPrim);
		offset(6); cout << Themes << endl;
		offset(6);  cout << Exit << endl;
		sign(13);
		break;
	case 1:
			if (lang == l_PL) 
			{
				lang = l_ENG;
			}
			else if(lang == l_ENG)
			{
				lang = l_PL;
			}
			language();
			system("cls");
		

		break;
	}

}
void themes()
{
	ENTER = false;
	system("cls");
	
	title();
	offset(6);  cout << NewGame << endl;
	offset(6);  cout << Stats << endl;
	offset(6);  cout << Language << endl;
	color(colSec);  offset(6);  cout << Themes << endl;
	color(colThird);
	switch (x_axis)
	{
	case 0:
		offset(5); cout << "      < PINKY & PUNKY >" << endl;
		break;
	case 1:
		offset(6); cout << " < ELEGANT >" << endl;
		break;

	case 2:
		offset(5); cout << "    <THE CAKE IS A LIE >" << endl;
			break;
	case 3:
		offset(6); cout << "< TRUE MEN >" << endl;
		break;
	case 4:
		offset(5); cout << "   < WINDOWS HATES YOU >" << endl;
		break;
	case 5:
		offset(6); cout << "  < MARIO >" << endl;
		break;
	case 6:
		offset(5); cout << "       < GREEN MARIO >" << endl;
		break;
	case 7:
		offset(5); cout << "       < GOLDEN TIME >" << endl;
		break;
	case 8:
		offset(6); cout << " < MODERN >" << endl;
		break;
	case 9:
		offset(6); cout << " < DEFAULT >" << endl;
		break;
	case 10:
		offset(6); cout << "< I'M BLIND >" << endl;
		break;
	case 11:
		offset(6); cout << "< BLACKBOARD >" << endl;
		break;
	case 12:
		offset(6); cout << " < MATRIX >" << endl;
		break;
	}
	
	color(colPrim);
	offset(6);  cout << Exit << endl;
	sign(13);
	navigation(0, 12, false);
	if (ENTER == true)
	{
		switch (x_axis)
		{
		case 0:
			colPrim = 7;
			colSec = 11;
			colThird = 13;
			active_theme = 0;
			break;
		case 1:
			colPrim = 7;
			colSec = 6;
			colThird = 8;
			active_theme = 1;
			break;

		case 2:
			colPrim = 7;
			colSec = 11;
			colThird = 6;
			active_theme = 2;
			break;
		case 3:
			colPrim = 95;
			colSec = 91;
			colThird = 92;
			active_theme = 3;
			break;
		case 4:
			colPrim = 31;
			colSec = 23;
			colThird = 113;
			active_theme = 4;
			break;
		case 5:
			colPrim = 4;
			colSec = 1;
			colThird = 12;
			active_theme = 5;
			break;
		case 6:
			colPrim = 2;
			colSec = 1;
			colThird = 10;
			active_theme = 6;
			break;
		case 7:
			colPrim = 96;
			colSec = 111;
			colThird = 110;
			active_theme = 7;
			break;
		case 8:
			colPrim = 240;
			colSec = 241;
			colThird = 244;
			active_theme = 8;
			break;
		case 9:
			colPrim = 7;
			colSec = 7;
			colThird = 7;
			active_theme = 9;
			break;
		case 10:
			colPrim = 0;
			colSec = 0;
			colThird = 8;
			active_theme = 10;
			break;
		case 11:
			colPrim = 39;
			colSec = 39;
			colThird = 39;
			active_theme = 11;
			break;
		case 12:
			colPrim = 2;
			colSec = 2;
			colThird = 10;
			active_theme = 12;
			break;
		}
		
	}
	
}
void exit()
{
	EXIT = true;
}
void startup()
{
	title();
	y_axis = 4;
	color(colThird); offset(6);  cout << NewGame_selected << endl;
	color(colPrim);  offset(6);  cout << Stats << endl;
	offset(6);  cout << Language << endl;
	offset(6); cout << Themes << endl;
	offset(6);  cout << Exit << endl;
	sign(14);
}
void draw_menu()
{
	navigation(4, 0, false);
	title();
	color(colPrim);
	switch (y_axis)
	{
	case 0:
		offset(6);  cout << NewGame << endl;
		offset(6);  cout << Stats << endl;
		offset(6);  cout << Language << endl;
		offset(6); cout << Themes << endl;
		color(colThird);  offset(6);  cout << Exit_selected << endl;
		color(colPrim);
		break;
	case 1:
		offset(6);  cout << NewGame << endl;
		offset(6);  cout << Stats << endl;
		offset(6); cout  << Language <<endl;
		color(colThird); offset(6);  cout << Themes_selected << endl;
		color(colPrim); offset(6);  cout << Exit << endl;
		break;
	case 2:
		offset(6);  cout << NewGame << endl;
		offset(6);  cout << Stats << endl;
		color(colThird); offset(6);  cout << Language_selected << endl;
		color(colPrim); offset(6); cout << Themes << endl;
		offset(6);  cout << Exit << endl;
		break;
	case 3:
		offset(6);  cout << NewGame << endl;
		color(colThird); offset(6);  cout << Stats_selected << endl;
		color(colPrim);  offset(6);  cout << Language << endl;
		offset(6); cout << Themes << endl;
		offset(6);  cout << Exit << endl;
		break;
	case 4:
		color(colThird); offset(6);  cout << NewGame_selected << endl;
		color(colPrim); offset(6);  cout << Stats << endl;
		offset(6);  cout << Language << endl;
		offset(6); cout << Themes << endl;
		offset(6);  cout << Exit << endl;
		break;
	}
	sign(14);
	if (ENTER == true)
	{
		switch (y_axis)
		{
		case 0:
			exit();
			break;
		case 1:
			x_axis = active_theme;
			do {
				themes();
			} while (ENTER == false);
			system("cls");
			startup();
			break;
			break;
		case 2:
			do {
				languages();
			} while (ENTER == false);
			system("cls");
			startup();
			break;
		case 3:
			system("cls");
			do
			{
				score_board();
				navigation(0,0, false);
			} while (ESC == false);
			startup();
			break;
		case 4:
			clear_field();
			system("cls");
			start_scene();
			do
			{
				draw_scene();
			} while (ESC == false);
			Player1_points = 0;
			Player2_points = 0;
			player = Player_O;
			system("cls");
			startup();
			break;
		}
		ENTER = false;
		ESC = false;
		
	}

}
