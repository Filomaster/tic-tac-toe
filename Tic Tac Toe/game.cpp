#pragma comment(lib, "winmm.lib")

#include<Windows.h>
#include<iostream>
#include<string>
#include<conio.h>

#include "functions.h"
#include "externs.h"
#include "struct.h"

using namespace std;

FIELD_TYPE field_t[3][3] = { {field_Empty, field_Empty, field_Empty},
						     {field_Empty, field_Empty, field_Empty}, 
						     {field_Empty, field_Empty, field_Empty} };
FIELD_STATUS field_s[3][3] = { { blank, blank, blank },
							   { blank, blank, blank },
							   { blank, blank, blank } };
PLAYER player = Player_O;

string line_horizontal = "::::::::::::::::::::::::::";
string line_vertical = "::";
string f_empty[5] = {"       ", "       ", "       ", "       ", "       " }; // " "
string f_empty_selected[5] = { "%%   %%", "%     %", "%     %", "%     %", "%%   %%" }; // " [ ] "
string f_circle[5] = {"       ", "   o   ", " o   o ", "   o   ", "       " };// "o"
string f_circle_crossed_horizontal[5] = { "       ", "   o   ", "#######", "   o   ", "       " }; // "--"
string f_circle_crossed_vertical[5] = {"   #   ", "   #   ", " o # O ", "   #   ", "   #   ", }; // " | "
string f_circle_crossed_slop_left[5] = {"      #", "   o # ", " o # o ", "  # o  ", "#      "}; // " / "
string f_circle_crossed_slop_right[5] = {"#      ", " # o   ", " o # o ", "   o # ", "      #"}; // " \ "
string f_circle_selected[5] = { "%%   %%", "%  o  %", "%o   o%", "%  o  %", "%%   %%" }; // " [o] "
string f_cross[5] = { "       ", " o   o ", "   o   ", " o   o ", "       ", }; // "X"
string f_cross_crossed_horizontal[5] = { "       ", " o   o ", "#######", " o   o ", "       ", }; // "--"
string f_cross_crossed_vertical[5] = {"   #   ", " o # o ", "   #   ", " o # o ", "   #   ", }; // " | "
string f_cross_crossed_slop_left[5] = {"      #", " o   # ", "   #   ", "  #  o ", "#      "}; // " / "
string f_cross_crossed_slop_right[5] = { "#      ", " #   o ", "   #   ", " #   o ", "      #" }; // " \ "
string f_cross_selected[5] = { "%%   %%", "%o   o%", "%  o  %", "%o   o%", "%%   %%" }; // " [x] "

bool Round_end = false;
bool IsWin = false;
bool IsTie = false;

int Player1_points;
int Player2_points;
int tie;


void movement()
{
	ENTER = false;
	navigation(2, 2, true);
	
	system("cls");
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			field_s[i][j] = blank;
		}
	}
	field_s[y_axis][x_axis] = selected;
	if (ENTER == true)
	{
		if (field_t[y_axis][x_axis] == field_Empty) 
		{
			switch (player)
			{
			case Player_X:
				field_t[y_axis][x_axis] = field_Cross;
				player = Player_O;
				break;
			case Player_O:
				field_t[y_axis][x_axis] = field_Circle;
				player = Player_X;
				break;
			}
		}
		else
		{
			PlaySound(TEXT("bin\\beeper.wav"), NULL, SND_ASYNC);
		}
	}
}
void clear_field()
{
	for (int i = 0; i < 3; i++) 
	{

	
		for (int j = 0; j < 3; j++)
		{
			field_s[i][j] = blank;
			field_t[i][j] = field_Empty;
		}
	}
}
void game_state() 
{
	IsWin = false;
	for (int j = 0; j < 3; j++) {
		if (field_t[0][j] == field_Cross || field_t[0][j] == field_Circle)
		{
			if (field_t[0][j] == field_t[1][j] && field_t[1][j] == field_t[2][j])
			{
				for (int i = 0; i < 3; i++)
				{
					field_s[i][j] = crossed_vertically;
				}
				Round_end = true;
				IsWin = true;
			}
		}
	}
	for (int j = 0; j < 3; j++) {
		if (field_t[j][0] == field_Cross || field_t[j][0] == field_Circle)
		{
			if (field_t[j][0] == field_t[j][1] && field_t[j][1] == field_t[j][2])
			{
				for (int i = 0; i < 3; i++)
				{
					field_s[j][i] = crossed_horizontally;
				}
				Round_end = true;
				IsWin = true;
			}
		}
	}
	if (field_t[0][0] == field_Cross || field_t[0][0] == field_Circle)
	{
		if (field_t[0][0] == field_t[1][1] && field_t[1][1] == field_t[2][2])
		{
			for (int i = 0; i < 3; i++)
			{
				field_s[i][i] = crossed_right_slop;
			}
			Round_end = true;
			IsWin = true;
		}
	}
	if (field_t[0][2] == field_Cross || field_t[0][2] == field_Circle)
	{
		
		if (field_t[0][2] == field_t[1][1] && field_t[1][1] == field_t[2][0]) 
		{

			field_s[0][2] = crossed_left_slop;
			field_s[1][1] = crossed_left_slop;
			field_s[2][0] = crossed_left_slop;
			Round_end = true;
			IsWin = true;
		}
		
	}
	tie = 0;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (field_t[i][j] == field_Cross || field_t[i][j] == field_Circle)
			{
				tie++;
			}
		}
	}
	if (IsWin == false)
	{
		if (tie == 9)
		{
			Round_end = true;
			IsTie = true;
		}
	}
	
}
void field_checker(int line, int field, int char_line)
{
	//switch (field_t[line][field])
	//{
	//case field_Empty:
		switch (field_t[line][field])
		{
		case field_Empty:
			switch (field_s[line][field])
			{
			case blank:
				cout << f_empty[char_line];
				break;

			case selected:
				cout << f_empty_selected[char_line];
				break;
			}
			break;
		case field_Circle:
			color(colSec);
			switch (field_s[line][field])
			{
			case blank:
				cout << f_circle[char_line];
				break;
			case selected:
				cout << f_circle_selected[char_line];
				break;
			case crossed_horizontally:
				cout << f_circle_crossed_horizontal[char_line];
				break;
			case crossed_vertically:
				cout << f_circle_crossed_vertical[char_line];
				break;
			case crossed_left_slop:
				cout << f_circle_crossed_slop_left[char_line];
				break;
			case crossed_right_slop:
				cout << f_circle_crossed_slop_right[char_line];
				break;

			}
			break;
		case field_Cross:
			color(colThird);
			switch (field_s[line][field])
			{
			case blank:
				cout << f_cross[char_line];
				break;
			case selected:
				cout << f_cross_selected[char_line];
				break;
			case crossed_horizontally:
				cout << f_cross_crossed_horizontal[char_line];
				break;
			case crossed_vertically:
				cout << f_cross_crossed_vertical[char_line];
				break;
			case crossed_left_slop:
				cout << f_cross_crossed_slop_left[char_line];
				break;
			case crossed_right_slop:
				cout << f_cross_crossed_slop_right[char_line];
				break;
			}
			break;
		}
		
		//break;

	//}
}
void title_line()
{
	color(colPrim);
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
	color(colSec);
	offset(4);	cout << " _______ _        _______           _______" << endl;
	offset(4); cout << "|__   __(_)      |__   __|         |__   __|" << endl;
	offset(4); cout << "   | |   _  ___     | | __ _  ___     | | ___   ___" << endl;
	offset(4); cout << "   | |  | |/ __|    | |/ _` |/ __|    | |/ _ \\ / _ \\ "<< endl;
	offset(4); cout << "   | |  | | (__     | | (_| | (__     | | (_) |  __/" << endl;
	offset(4); cout << "   |_|  |_|\\___|    |_|\\__,_|\\___|    |_|\\___/ \\___|" << endl;
	color(colThird);
	offset(10); cout << "by Filomaster" << endl;
	color(colPrim);
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
	cout << "    "; color(colSec); cout << Player << " 1"; offset(6); cout << Player1_points; color(colPrim); cout << " || "; color(colThird); cout << Player2_points; offset(6); cout << Player << " 2"; color(colPrim); cout << "" << endl;
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
}
void draw_line(int line_number)
{
	for (int i = 0; i < 5; i++)
	{
	
		for (int j = 0; j < 3; j++)
		{
			


			if (j == 0)
			{
				offset(6);
			}
			field_checker(line_number, j, i);
			color(colPrim);
			if (j != 2)
			{
			cout << "::";
			}
			else
			{
				cout << "" << endl;
			}
		}
	}
}
void turn()
{
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
	offset(6); cout<<"    ";
	if (player == Player_O)
	{
		color(colSec); cout << Player1_turn << endl;
	}
	if (player == Player_X)
	{
		color(colThird); cout << Player2_turn << endl;
	}
}
void start_scene()
{
	player = Player_O;
	field_s[1][1] = selected;
	x_axis = 1;
	y_axis = 1;
	title_line();
	cout << "" << endl;
	for (int i = 0; i < 3; i++)
	{
		draw_line(i);
		if (i < 2)
		{
			offset(6);  cout << line_horizontal << endl;
		}
	}
	turn();
}
void game_over()
{
	if (Round_end == true)
	{
		if (IsTie == false) 
		{
			PlaySound(TEXT("bin\\tie.wav"), NULL, SND_ASYNC);
			Sleep(1000);
			clear_field();
			if (player == Player_O)
			{
				Player2_points++;
			}
			if (player == Player_X)
			{
				Player1_points++;
			}
			Round_end = false;
			system("cls");
			start_scene();
		}
		else if (IsTie == true)
		{
			PlaySound(TEXT("bin\\beeper.wav"), NULL, SND_ASYNC);
			Sleep(1000);
			clear_field();
			Round_end = false;
			IsTie = false;
			system("cls");
			start_scene();
		}
	}
}
void draw_scene()
{
	//PlaySound(TEXT("bin\\music.wav"), NULL, SND_ASYNC);
	
	movement();
	title_line();
	cout << "" << endl;
	for (int i = 0; i < 3; i++)
	{
		game_state();
		draw_line(i);
		if (i < 2)
		{
			offset(6);  cout << line_horizontal << endl;
		}
	}
	turn();
	game_over();
	
}